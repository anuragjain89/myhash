class MyHash < Array

  def initialize
    @buffer = []
  end

  def [](key)
    super(@buffer.index(key)) if @buffer.include?(key)
  end

  def []=(key, val)
    @buffer.push(key) unless @buffer.include?(key)
    super(@buffer.index(key),val)
  end

end

